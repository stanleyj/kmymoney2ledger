# kmymoney2ledger

Converts KMyMoney2 files into plain-text ledger-cli files. Also supports other dialects such as beancount, ledger-py.

## Installation

Install [leiningen](https://leiningen.org/) first.

Then, download and compile the source code with:

    $ git clone https://gitlab.com/stanleyj/kmymoney2ledger
    $ cd kmymoney2ledger
    $ lein uberjar

inside the `target/uberjar` directory you will find the file `kmymoney2ledger-0.1.0-standalone.jar`.

## Usage

    $ java -jar kmymoney2ledger-0.1.0-standalone.jar path-to-kmymoney-file dialect output-ledger-file

## Options

1. path to KMyMoney2 .kmy file
- dialect, optional. If no dialect is given, `ledger-cli` is assumed
- path to output file, optional. However, when using this option, a dialect must be specified as well


### Bugs

Only tested with dialect `beancount`.

## License

Copyright © 2019 Stanley Jaddoe

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
