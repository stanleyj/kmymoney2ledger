(defproject kmymoney2ledger "0.1.0-SNAPSHOT"
  :description "Converts KMyMoney2 .kmy XML files into ledger data files"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-time "0.14.2"]
                 [org.clojure/data.zip "0.1.2"]]
  :main ^:skip-aot kmymoney2ledger.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
