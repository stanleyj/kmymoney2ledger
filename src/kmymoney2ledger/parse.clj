(ns kmymoney2ledger.parse
  (:require [clojure.xml :as xml]
            [clojure.java.io :as io]
            [clojure.zip :as zip]
            [clj-time.format :as ctf]
            [clojure.data.zip.xml :refer [attr xml->]]))

(defn parse-date [d]
  (try
   (ctf/parse (ctf/formatters :date) d)
   (catch Exception _
     nil)))

(defn xml->map
  [root f & path]
  (into {} (map f (apply xml-> root path))))

(defn xml->vec
  [root f & path]
  (mapv f (apply xml-> root path)))

; keyvaluepairs

(defn handle-kv-pair
  [p]
  [(keyword (str "kvp-" (clojure.string/lower-case (attr p :key))))
   (attr p :value)])

(defn key-value-pairs
  [root]
  (xml->map root handle-kv-pair :KEYVALUEPAIRS :PAIR))

; account

(defn account->index
  [a]
  [(attr a :id)
   (merge (key-value-pairs a)
          {:name (attr a :name)
           :description (attr a :description)
           :commodity (attr a :currency)
           :opened (parse-date (attr a :opened))
           :id (attr a :id)
           :parent-id (attr a :parentaccount)})])

(defn accounts-index
  [root]
  (xml->map root account->index :ACCOUNTS :ACCOUNT))

(defn account-hierarchy
  ([acc-idx account-id]
   (account-hierarchy acc-idx account-id []))
  ([acc-idx account-id result]
   (if-let [account (acc-idx account-id)]
     (recur acc-idx (:parent-id account) (conj result account))
     result)))

(defn account-categories-index
  [accounts]
  (zipmap (keys accounts)
          (map (partial account-hierarchy accounts)
               (keys accounts))))

; payee

(defn payee->name
  [p]
  [(attr p :id)
   (attr p :name)])

(defn payees-index
  [root]
  (xml->map root payee->name :PAYEES :PAYEE))

; split

(defn shares->amount
  [s]
  (read-string s))

(def reconcile-state
  {"0" nil
   "1" :checked
   "2" :cleared})

(defn xml->split
  [categories payees s]
  {:amount (shares->amount (attr s :shares))
   :category (categories (attr s :account))
   :payee (payees (attr s :payee))
   :reconcile-state (reconcile-state (attr s :reconcileflag))
   :reconcile-date (parse-date (attr s :reconciledate))
   :id (attr s :id)
   :comment (attr s :memo)})

; transaction

(defn xml->transaction
  [categories payees t]
  (merge (key-value-pairs t)
         {:postdate (parse-date (attr t :postdate))
          :entrydate (parse-date (attr t :entrydate))
          :commodity (attr t :commodity)
          :comment (attr t :memo)
          :id (attr t :id)
          :splits (xml->vec t (partial xml->split categories payees)
                            :SPLITS :SPLIT)}))

; read KMyMoney file

(defn kmymoney-xml
  [filename]
  (-> filename
      io/input-stream
      java.util.zip.GZIPInputStream.
      xml/parse
      zip/xml-zip))

(defn kmymoney->map
  [filename]
  (let [root (kmymoney-xml filename)
        accounts (accounts-index root)
        categories (account-categories-index accounts)
        payees (payees-index root)
        transactions (xml->vec root (partial xml->transaction categories payees)
                               :TRANSACTIONS :TRANSACTION)]
    {:accounts (vals categories)
     :transactions transactions}))
