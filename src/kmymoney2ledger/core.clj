(ns kmymoney2ledger.core
  (:gen-class)
  (:require [kmymoney2ledger.parse :as parse]
            [kmymoney2ledger.export :as export]))

(defn dialects [d]
  (get {"pyledger" :ledger-py
        "ledger-py" :ledger-py
        "ledger-cli" :ledger-cli
        "ledger" :ledger-cli
        "beancount" :beancount
        } d :ledger-cli))

(defn -main
  ([filename]
   (-main filename "ledger"))
  ([filename dialect]
   (print (export/export-ledger (parse/kmymoney->map filename)
                                (dialects dialect))))
  ([filename dialect output]
   (spit output (export/export-ledger (parse/kmymoney->map filename)
                                      (dialects dialect)))))
