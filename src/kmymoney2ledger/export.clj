(ns kmymoney2ledger.export
  (:require [clj-time.coerce :refer [to-long]]))

(defn clean-string [s]
  (-> s
      (clojure.string/replace #"\n" " ")
      (clojure.string/replace #"\s+" " ")
      clojure.string/trim))

(defn note [s]
  (clean-string (:comment s)))

(defn account-type [a dialect]
  (get-in {:beancount {"Asset" "Assets"
                       "Income" "Income"
                       "Expense" "Expenses"
                       "Liability" "Liabilities"
                       "Equity" "Equity"}
           :ledger {"Asset" "Assets"
                    "Income" "Income"
                    "Expense" "Expenses"
                    "Liability" "Liabilities"
                    "Equity" "Equity"}}
          [dialect a] a))

(defn transaction-status [a dialect]
  (get-in {:beancount {nil "!"
                       :cleared "*"
                       :checked "*"
                       }}
          [dialect a]))

(defn category [dialect s]
  (let [accounts (reverse (map :name s))]
    (case dialect
      :beancount
      (-> (clojure.string/join ":" (update (vec accounts) 0
                                           account-type dialect))
          (clojure.string/replace #"[^0-9A-Za-z:\- ]" "")
          (clojure.string/replace #" +" "-"))
      ;default
      (-> (clojure.string/join ":" (update (vec accounts) 0
                                           account-type dialect))
          (clojure.string/replace #" +" " ")))))

(defn date [d]
  (clj-time.format/unparse (clj-time.format/formatters :date) d))

(defn split
  [dialect t s]
  (let [amount (float (:amount s))
        category (category dialect (:category s))
        commodity (:commodity t)
        first-split (first (:splits t))
        tr-note (note first-split)
        note (note s)
        note-eq-tr? (or (empty? note) (= note tr-note))]
    (case dialect
      :ledger-py
      (if note-eq-tr?
        (format "  %s %.2f%n" category amount)
        (format "# %s%n  %s %.2f%n" note category amount))
      :beancount
      (if note-eq-tr?
        (format "  %s %.2f %s%n" category amount commodity)
        (format "  %s %.2f %s ;%s%n" category amount commodity note))
      ;default ledger-cli
      (if note-eq-tr?
        (format "  %s  %.2f %s%n" category amount commodity)
        (format "  %s  %.2f %s ; %s%n" category amount commodity note)))))

(defn transaction
  [dialect t]
  (let [first-split (first (:splits t))
        splits (clojure.string/join (map (partial split dialect t) (:splits t)))
        date (date (:postdate t))
        payee (:payee first-split)
        tr-flag (transaction-status (:reconcile-state first-split) dialect)
        note (note t)
        payee-note (clean-string (str payee " " (:comment note)))]
    (case dialect
      :ledger-py
      (format "%s %s%n%s%n" date payee-note splits)
      :beancount
      (if payee
        (format "%s %s \"%s\" \"%s\"%n%s%n" date tr-flag payee note splits)
        (if (clojure.string/blank? note)
          (format "%s %s%n%s%n" date tr-flag splits)
          (format "%s %s \"%s\"%n%s%n" date tr-flag note splits)))
      ;default ledger-cli
      (format "%s %s%n%s%n" date payee-note splits))))

(defn directives
  [dialect kmm]
  (let [accounts (:accounts kmm)]
    (case dialect
      :beancount
      (clojure.string/join
       "\n"
       (map #(format "%s open %s %s"
                     (date (:opened (second (rseq %))))
                     (category dialect %)
                     (:commodity (first %)))
            accounts))
      ;default ledger-cli
      ""
      )))

(defn export-ledger
  [kmymoney-map dialect]
  (str (directives dialect kmymoney-map) "\n"
       (clojure.string/join
        (map (partial transaction dialect)
             (sort-by (comp to-long :postdate) <
                      (:transactions kmymoney-map))))
       "\n"))
